(function () {
    activateTemplate("Hi");
    activateTemplate("Hello");
    activateTemplate("Saletti");
    function activateTemplate(greeting) {
        // load template
        var template = document.querySelector("#mytemplate");
        // deep clone template
        var clone = document.importNode(template.content, true);
        // populate data
        clone.querySelector("#greeting").innerHTML = greeting;
        // add to DOM
        var host = document.querySelector("#host");
        host.appendChild(clone);
    }
})();
//# sourceMappingURL=bundle.js.map