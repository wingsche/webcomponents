(function () {
    hello("Hello");

    /**
     * Displays a {greeting} world on the page.
     * @param {string} greeting The greeting to display. 
     */
    function hello(greeting) {
        const elm = document.querySelector("p");
        const text = `${greeting} world!`;
        elm.innerHTML = text;
    }
})();