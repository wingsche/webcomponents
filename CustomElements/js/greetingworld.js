// custom element defined with the pure javascript method
class GreetingWorld extends HTMLElement {
    // called when attached to the DOM
    connectedCallback() {
        this.render();
    }

    render() {
        const formula = this.getAttribute("formula");
        this.innerHTML = `<p>${formula} World</p>`;
    }
}
window.customElements.define("greeting-world", GreetingWorld);
