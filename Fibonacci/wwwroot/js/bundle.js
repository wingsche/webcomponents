var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var FibonacciCounter = /** @class */ (function (_super) {
    __extends(FibonacciCounter, _super);
    function FibonacciCounter() {
        var _this = _super.call(this) || this;
        _this._lastValue = 0;
        _this._currentValue = 1;
        _this._iteration = 1;
        _this._$iteration;
        _this._$value;
        _this._intervalHandler;
        _this._$root = _this.attachShadow({ "mode": "open" });
        return _this;
    }
    FibonacciCounter.prototype.connectedCallback = function () {
        this._$root.innerHTML = "\n            <style>\n                .fb-container {\n                    width: 250px;\n                    margin: auto;\n                    border: solid 1px;\n                    padding: 20px;\n                    float: left;\n                }\n            </style>\n            <div class=\"fb-container\">\n                <h1>Fibonacci</h1>\n                <p>Iteration: <span id=\"iteration\"></span></p>\n                <p>Value:     <span id=\"value\"></span></p>\n            </div>\n        ";
        this._$iteration = this._$root.querySelector("#iteration");
        this._$value = this._$root.querySelector("#value");
        this.setInterval(this.getAttribute("interval"));
        this.render();
    };
    Object.defineProperty(FibonacciCounter, "observedAttributes", {
        get: function () {
            return ["interval"];
        },
        enumerable: true,
        configurable: true
    });
    FibonacciCounter.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
        if (name === "interval") {
            this.setInterval(newValue);
        }
    };
    FibonacciCounter.prototype.disconnectedCallback = function () {
        console.log("Removing interval listener");
        clearInterval(this._intervalHandler);
    };
    FibonacciCounter.prototype.setInterval = function (value) {
        var _this = this;
        if (this._intervalHandler) {
            clearInterval(this._intervalHandler);
        }
        if (value > 0) {
            this._intervalHandler = setInterval(function () { return _this.render(); }, value);
        }
    };
    FibonacciCounter.prototype.render = function () {
        this._$iteration.innerHTML = this._iteration++;
        this._$value.innerHTML = this._currentValue;
        this.calculateNextValue();
    };
    FibonacciCounter.prototype.calculateNextValue = function () {
        var nextValue = this._lastValue + this._currentValue;
        this._lastValue = this._currentValue;
        this._currentValue = nextValue;
    };
    return FibonacciCounter;
}(HTMLElement));
window.customElements.define("fibonacci-counter", FibonacciCounter);
//# sourceMappingURL=bundle.js.map