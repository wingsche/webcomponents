class FibonacciCounter extends HTMLElement
{
    constructor(){
        super();
        this._lastValue = 0;
        this._currentValue = 1;
        this._iteration = 1;
        this._$iteration;
        this._$value;
        this._intervalHandler;
        this._$root = this.attachShadow({"mode": "open"});
    }

    connectedCallback(){
        this._$root.innerHTML = `
            <style>
                .fb-container {
                    width: 250px;
                    margin: auto;
                    border: solid 1px;
                    padding: 20px;
                    float: left;
                }
            </style>
            <div class="fb-container">
                <h1>Fibonacci</h1>
                <p>Iteration: <span id="iteration"></span></p>
                <p>Value:     <span id="value"></span></p>
            </div>
        `;
        this._$iteration = this._$root.querySelector("#iteration");
        this._$value = this._$root.querySelector("#value");
        this.setInterval(this.getAttribute("interval"));
        this.render();
    }

    static get observedAttributes(){
        return ["interval"];
    }

    attributeChangedCallback(name, oldValue, newValue){
        if (name === "interval"){
            this.setInterval(newValue);
        }
    }

    disconnectedCallback(){
        console.log("Removing interval listener")
        clearInterval(this._intervalHandler);
    }

    setInterval(value){
        if (this._intervalHandler){
            clearInterval(this._intervalHandler);            
        }
        if (value > 0){
            this._intervalHandler = setInterval(() => this.render(), value);
        }
    }
    render(){
        this._$iteration.innerHTML = this._iteration++;
        this._$value.innerHTML = this._currentValue;
        
        this.calculateNextValue();

    }

    calculateNextValue(){
        let nextValue = this._lastValue + this._currentValue;
        this._lastValue = this._currentValue;
        this._currentValue = nextValue;
    }

}

window.customElements.define("fibonacci-counter", FibonacciCounter)