var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// custom element defined with the pure javascript method
var GreetingWorld = /** @class */ (function (_super) {
    __extends(GreetingWorld, _super);
    function GreetingWorld() {
        var _this = _super.call(this) || this;
        // attach a shadow root to our element
        _this.shadowRoot = _this.attachShadow({ mode: 'open' });
        return _this;
    }
    // called when attached to the DOM
    GreetingWorld.prototype.connectedCallback = function () {
        this.render();
    };
    GreetingWorld.prototype.render = function () {
        var formula = this.getAttribute("formula");
        // append any html to the shadow root
        this.shadowRoot.innerHTML = "<p>" + formula + " World</p>";
    };
    return GreetingWorld;
}(HTMLElement));
window.customElements.define("greeting-world", GreetingWorld);
//# sourceMappingURL=bundle.js.map