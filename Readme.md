﻿# Overwiev
The Goal of Web Components is to build reusable components for the web. A Web Component allows to encapsulate Html, Css and JavaScript. Web Components rely on the web standards only, so there are no third-party dependencies.  

The Web Components specification has four parts:

- Html Templates
- Custom Elements
- Shadow DOM
- Html Imports

Popular libraries which build on top of web components are:

- [Polymer](https://github.com/Polymer/polymer)
- [SkateJS](https://github.com/skatejs/skatejs)
- [x-tag](https://github.com/x-tag/x-tag)

-----------------------
# Getting Started
## Prerequisite

- Visual Studio 2017 (>=15.3)
- VS Extensions
    - [ASP.NET Core Template Pack 2017.3](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.ASPNETCoreTemplatePack20173) contains StaticWeb
    - [JavaScript Transpiler](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.JavaScriptTranspiler)
    - [Web Essentials 2017](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebExtensionPack2017) contains Browser Link (and more)
    - [Error Catcher II](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.ErrorCatcherII) displays icon for ESLint errors 

## Visual Studio 2017

- Create Project: New Project > ASP.NET Core Web Application > Static Web

- Fix **Downgrade Warning** in Visual Studio 2017.5 by explicitly declaring dependencies:

```code
    <ItemGroup>
        <PackageReference Include="AspNetCore.StaticSiteHelper" Version="1.0.9" />
        <PackageReference Include="Microsoft.AspNetCore" Version="2.0.0" />
        <PackageReference Include="Microsoft.AspNetCore.Server.IISIntegration" Version="2.0.0" />
        <PackageReference Include="Microsoft.AspNetCore.Server.Kestrel" Version="2.0.0" />
        <PackageReference Include="Microsoft.AspNetCore.StaticFiles" Version="2.0.0" />
        <PackageReference Include="Microsoft.VisualStudio.Web.BrowserLink" Version="2.0.0" />
    </ItemGroup>
```
- Enable Browser Link in Browser Link Dropdown Standard Toolbar

-----------------------
# VS Support
The following projects show the support of Visual Studio for Frontend Development.
## StaticWeb
This is an empty project build from the Static Web template: there is no Bootstrap, no JQuery, no WebPack, no Transpiler and no Bundler installed. Its a simple responsive web site.

## HelloWorld
Same as ``StaticWeb`` with some logic in ``wwwroot\js\site.js``.

Used for demonstration

- Save and Reload (Browser Link)
- Client-side debugging
- Icon on ESLint errors

## Transpilation

Same as ``HelloWorld`` but transpiles the ES2015 (ES6) code to ES5 using built-in support from TypeScript. The source file ``js\site.js`` is transpiled to ``wwwroot\js\bundle.js``. The Transpilation is configured in ``tsconfig.js``.

- Differences
    - var instead of const
    - string contatination instead of template string
- Save and Reload **not** working?
- Client-side debugging in source file
- Comments on functions
- Intellisence due to type information in commented functions
- Type checking (tsconfig: checkJs)

-----------------------
# Web Components

## HtmlTemplates
 - Defined by new ``<template>`` element
     - Contains other elements/scripts
     - Not rendered/executed/evaluated at page load
     - Child nods not accessable by document.getElementById() or querySelector()
     - Placed anywhere on the page
   
 - Used by instatiating the template at runtime
     - Load template
     - Deep-clone template content
     - Populate data
     - Add to DOM


### Links
- [Tutorial](https://www.html5rocks.com/en/tutorials/webcomponents/template/)
- [Browser Support](https://caniuse.com/#search=template)
- [Specification](https://html.spec.whatwg.org/multipage/scripting.html#the-template-element)
## Custom Elements
- Lets us define our own HTML Elements to avoid [div soups](https://stackoverflow.com/questions/9845011/are-custom-elements-valid-html5)
- Gives the content meaning 
- Name needs a dash
- Use namespace approach to avoid naming collisions
- Four ways to use an element
    - as markup: ``<greeting-world/>`` 
    - with new operator: ``var elm = new GreetingWorld`` (must be appended to the DOM)
    - using createElement: ``var elm = document.createElement('greeting-worl')``  (must be appended to the DOM)
    - in innerHtml = ``elm.innerHtml = '<greeting-world/>'`` 
- Transpilation needs [native shim](https://github.com/webcomponents/custom-elements/blob/master/src/native-shim.js)
### Links
- [Tutorial](https://developers.google.com/web/fundamentals/web-components/customelements)
- [Browser Support](https://caniuse.com/#search=Custom%20Elements%20v1)
- [Specification](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements)
- [Why native-shim is needed](https://github.com/webcomponents/custom-elements#known-issues)
## Shadow DOM
- Create sum-DOM tree for a specific element
- Provides encapsulates of the sub-DOM
    - don't inherit styles from the application
- sub-DOM is attached to a node also known as host
### Links
- [Tutorial](https://developers.google.com/web/fundamentals/web-components/shadowdom)
- [Browser Support](https://caniuse.com/#search=Shadow%20DOM%20v1)
- [Specification](https://dom.spec.whatwg.org/#shadow-trees)

## HTML Imports
- Bundles Html, Css and JavaScript
- nativly only supported by Chrome,[Polyfill](https://github.com/webcomponents/webcomponentsjs) needed to support other browsers.
### Links
- [Tutorial](https://www.html5rocks.com/en/tutorials/webcomponents/imports/)
- [Browser Support](https://caniuse.com/#search=Html%20imports)
- [Specification](https://w3c.github.io/webcomponents/spec/imports/)
