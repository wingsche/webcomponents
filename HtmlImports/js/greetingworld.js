// custom element defined with the pure javascript method
class GreetingWorld extends HTMLElement {
    constructor() {
        super(); // always call super() first in the ctor.

        // attach a shadow root to our element
        this.shadowRoot = this.attachShadow({ mode: 'open' });
    }

    // called when attached to the DOM
    connectedCallback() {
        this.render();
    }

    render() {
        const formula = this.getAttribute("formula");
        // append any html to the shadow root
        this.shadowRoot.innerHTML = `<p>${formula} World</p>`;
    }
}
window.customElements.define("greeting-world", GreetingWorld);
