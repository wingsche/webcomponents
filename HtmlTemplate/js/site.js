(function () {
    activateTemplate("Hi");
    activateTemplate("Hello");
    activateTemplate("Saletti");

    function activateTemplate(greeting) {
        // load template
        const template = document.querySelector("#mytemplate");
        // deep clone template
        const clone = document.importNode(template.content, true);

        // populate data
        clone.querySelector("#greeting").innerHTML = greeting;

        // add to DOM
        const host = document.querySelector("#host");
        host.appendChild(clone);
    }
})();