(function () {
    hello("Hello");

    function hello(greeting) {
        const elm = document.querySelector("p");
        const text = `${greeting} world!`;
        elm.innerHTML = text;
    }
})();